#!/usr/bin/env node
/***********************************************************
* pcan.js
* Author: Anelise Chapa 
************************************************************
* Description: 
* 
************************************************************/

/***********************************************************
* Input Packages
************************************************************/
const electron = require('electron');
const url = require('url');
const path = require('path');
const Can = require('@csllc/cs-canbus-universal');
const { createPublicKey } = require('crypto');
const fs = require('fs');
const encoding = require("encoding");
const { stringify } = require('querystring');
const { exit, addListener } = require('process');
const{app, BrowserWindow, Menu} = electron;

/***********************************************************
* Constant definitions
************************************************************/

let mainWindow;

const portsMenuTemplate = [
	{
	  label:'File', 
	  submenu: [
		{
		  label: 'Connect to can sniffers'
		}
	  ]
	}
  ]
  
  let can = new Can({
	canRate: 1000000
  });

  
let k_data 				= []
let k_frame_id 			= []

const CAN_PACKET_ID   =  0;
const CAN_PACKET_DATA =  2;
const CAN_PACKET_LEN  =  1;
const ERR_NOT_FOUND   = -1;


const PARA_MSGID_HIGH                 = 1;//High priority data frame
const PARA_MSGID_LOW				  = 2;//Low priority data frame
const PARA_MSGID_ANALOG_LOW1		  =	3;//Analog
const PARA_MSGID_ANALOG_LOW2		  =	4;//Analog
const PARA_MSGID_INVALID			  =	5;//Invalid frame
const PARA_MSGID_ANALOG_LOW3		  =	6;//Analog
const PARA_MSGID_PARAM_SYN		      = 7;//parameter synchronization
const PARA_MSGID_ANOLOG_BATT1		  =	8;//Dual battery management
const PARA_MSGID_ANOLOG_BATT2		  =	9;//Dual battery management
const PARA_MSGID_ANOLOG_BATT3		  =	10;//Dual battery management
const PARA_MSGID_HIGHSN			      =	11;//High priority data frame, send SN code
const PARA_MSGID_HIGHSNREP		      =	12;//High priority data frame, the SN code will respond after locking the address
const PARA_MSGID_TIME_LOW			  =	13;//Low priority data frame

//PARA_MSGID_HIGH
let bParaAddr           = ERR_NOT_FOUND;   
let bSelfIsMaster       = ERR_NOT_FOUND;  
let bSelfOKToBeMaster   = ERR_NOT_FOUND;

let bSeftCanInvSupply   = ERR_NOT_FOUND;
let bSeftBpOk           = ERR_NOT_FOUND;   
let bLocalsupply        = ERR_NOT_FOUND; 
let bSeftCanInvOn       = ERR_NOT_FOUND; 
let bSeftCanBpOn        = ERR_NOT_FOUND;
let bSelfMBstate        = ERR_NOT_FOUND;
let bSelfOverLoad       = ERR_NOT_FOUND;

let bAmpTrace           = ERR_NOT_FOUND;
let bSeftCanBpSupply    = ERR_NOT_FOUND;
let bMaster1stPhaseLock = ERR_NOT_FOUND;
let bMasterorNotBak     = ERR_NOT_FOUND;
/*
UINT16 bSyssupply                 :2; // BIT00-BIT01 System switching result 0 Standby 1 Bypass 2 Inverter 3 Combined power supply
UINT16 bLclINVToBPEmer            :1;
UINT16 bLclBPToINVEmer            :1;
UINT16 bBpOffCmd                  :1; // Bypass shutdown command
UINT16 bInvOffCmd                 :1; // Inverter shutdown command
UINT16 bSelfINVOvLoadTimeoutFault :1; // The inverter overload time of the machine is up
UINT16 bLclXStartToDlyFlag        :1; // Switch delay start flag
UINT16 bSelfEOD                   :1;
UINT16 bEODPowerOffUPSTotal       :1;
UINT16 bLocalSupplyAndbInv        :1;
UINT16 bLocal2ndPhaseLock         :1;
UINT16 bRevd                      :4; // BIT4-BIT15 reserved

*/

let counter_delay = 0;
/***********************************************************
* function definitions
************************************************************/

function unpack_can_packet(packet_id, rx_data) 
{
	//mask packet id to get frame id
	let hex_str = packet_id.toString(10)
	let hex_num = parseInt(hex_str)
	hex_num = hex_num >> 6;

	//data to string
	//let data_str = rx_data.toString(16)
	//let data_num = parseInt(data_str)
	switch(hex_num){
		case PARA_MSGID_HIGH:
			let num1 = parseInt(rx_data[1].toString(16), 16)
			let num2 = 1
			num2 = num1 >>>6;
			//console.log(((parseInt(rx_data[1].toString(16)))>>> 1)& 0xff)
			//console.log(num2)
			bParaAddr 				= (parseInt(rx_data[0].toString(16), 16) >>> 0)     & 0x0003;
			bSelfIsMaster 			= (parseInt(rx_data[0].toString(16), 16) >>> 2)     & 0x0001;
			bSelfOKToBeMaster		= (parseInt(rx_data[0].toString(16), 16) >>> 3)     & 0x0001;
			bSeftCanInvSupply		= (parseInt(rx_data[0].toString(16), 16) >>> 4)     & 0x0001;
			bSeftBpOk           	= (parseInt(rx_data[0].toString(16), 16) >>> 5)     & 0x0001;
			bLocalsupply      		= (parseInt(rx_data[0].toString(16), 16) >>> 6)     & 0x0001; 
			bSeftCanInvOn     		= (parseInt(rx_data[0].toString(16), 16) >>> 7)     & 0x0001;
			bSeftCanBpOn     		= (parseInt(rx_data[1].toString(16), 16) >>> 0)     & 0x0001;
			bSelfMBstate     		= (parseInt(rx_data[1].toString(16), 16) >>> 1)     & 0x0001;
			bSelfOverLoad    		= (parseInt(rx_data[1].toString(16), 16) >>> 2)     & 0x0001;
			bAmpTrace        		= (parseInt(rx_data[1].toString(16), 16) >>> 3)     & 0x0001;
			bSeftCanBpSupply 		= (parseInt(rx_data[1].toString(16), 16) >>> 4)     & 0x0001;
			bMaster1stPhaseLock		= (parseInt(rx_data[1].toString(16), 16) >>> 5)     & 0x0001;
			bMasterorNotBak  		= (parseInt(rx_data[1].toString(16), 16) >>> 6)     & 0x0001;
			break;
		case PARA_MSGID_LOW:
			break;
		default:
			break;
	}
}

can.on('error', (err) => { console.error(err); });
can.on('open', () => { console.log('open'); });
can.on('close', () => { console.log('close'); });

let delay = 0;
can.on('data', function(msg) {

	if (delay < 50) {
		delay++;
		return;
	}
	delay = 0;
	let data_info = JSON.parse(JSON.stringify(msg)) 
	let data_ids   = Object.values(data_info)

	key_in_data   = Object.keys(msg)
	value_in_data = Object.values(msg)
	
	let packet_id     = value_in_data[CAN_PACKET_ID];
	let packet_len    = value_in_data[CAN_PACKET_LEN];
	let packet_data   = value_in_data[CAN_PACKET_DATA];
	/*let found_equal = ERR_NOT_FOUND;
	for(let k = 0; k < k_frame_id.length; k++)
	{
		if(k_frame_id[k] == packet_id){
			found_equal = k;
		}
	}
	
	//found new packet id
	if(found_equal == ERR_NOT_FOUND) {
		k_frame_id.push(packet_id);
		k_data.push(packet_data)
	}
	//change value for existing packet id
	else 
	{
		k_data[found_equal] = packet_data;
	}*/
	//console.log(packet_data.length)
	unpack_can_packet(packet_id, packet_data)

	process.stdout.write('\033c');
	console.log("Receiving info from UPS with addr: %d", bParaAddr)
	console.log("bSelfIsMaster: %d", bSelfIsMaster)
	console.log("bSelfOKToBeMaster: %d", bSelfOKToBeMaster)
	console.log("bSeftCanInvSupply: %d", bSeftCanInvSupply)
	console.log("bSeftBpOk: %d", bSeftBpOk)
	console.log("bLocalsupply: %d", bLocalsupply)
	console.log("bSeftCanInvOn: %d", bSeftCanInvOn)
	console.log("bSeftCanBpOn: %d", bSeftCanBpOn)
	console.log("bSelfMBstate: %d", bSelfMBstate)
	console.log("bMasterorNotBak:  %d",bMasterorNotBak)
	
  });


/************************************************************
 * @brief Main Program
 * 
 * @param None
 ************************************************************/


app.on('ready', function()
{
	let k_device_name	    = []
	let k_path 				= []
	let k_channel_condition = []
	let k_dev_id 			= []

	mainWindow = new BrowserWindow({});
	mainWindow.loadURL(url.format({
	pathname: path.join(__dirname, 'sniffer/src/mainWindow.html'), 
	protocol: 'file:', 
	slashes: true
	}))
	const portsMenu = Menu.buildFromTemplate(portsMenuTemplate);
	Menu.setApplicationMenu(portsMenu);

	//CAN functionality
	console.log("Hello i will look for all CAN available ports")
	can.list()
	.then(function(ports) 
	{ 
		let port_info = JSON.parse(JSON.stringify(ports)) 
		let txt_ids = Object.values(port_info)
		
		for(let i = 0; i < txt_ids.length; i++) 
		{
			key_in_port   = Object.keys(txt_ids[i])
			value_in_port = Object.values(txt_ids[i])
			for(let j = 0; j < key_in_port.length; j++) 
			{
				if(key_in_port[j] == "device_name") 
				{
					k_device_name.push(value_in_port[j]);
				}
				if(key_in_port[j] == "path") 
				{
					k_path.push(value_in_port[j]);
				}
				if(key_in_port[j] == "channel_condition") 
				{
					k_channel_condition.push(value_in_port[j])
				}
				if(key_in_port[j] == "id") 
				{
					k_dev_id.push(value_in_port[j]);
				}
			}
		}
		console.log("\nThe ports ids are: ")
		console.log(k_dev_id)
		console.log("\nThe ports paths are: ")
		console.log(port_info[1])
		for(let i = 0; i < k_dev_id.length; i++) {
			if(k_dev_id[i] == 'pcan-usb_81') {
				//can.autoOpen(ports[i])
			}
			if(k_dev_id[i] == 'pcan-usb_82') {
				//can.autoOpen(ports[i])
			}
		} 
		
		console.log('opening ', ports[1].path);
		return can.open(k_dev_id[1]);

		//while(1){	
		//}
	}).then(function() {
		can.write({ id: 0x10EF8003, ext: true, buf: Buffer.from([0x45, 0x00, 0x00, 0x04, 0x00, 0x00]) });
	  })
	.catch(function(err) {
	  // Something went wrong...
	  console.error(err);
	  can.close();
	  process.exit(-1);
	});
});

can.on('write', function(msg) {
	console.log('Write: ', msg);
});




